#pragma once
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <math.h>

using namespace std;

enum class BranchDirection
{
	Left,
	Central,
	Right
};

class BaseHolder
{

protected:
	const char* FormattingCharArray(const char* data);
	bool InsertChar(const char(&data)[10]);
public:
	BaseHolder();
	bool Insert(BaseHolder* data);
	bool Insert(const int& data);
	bool Insert(const char* data);
	bool Insert(const float& data);

	virtual BaseHolder* FindAtPos(int pos) 
	{
		list<BranchDirection> PathToBranch = CalculatePathToBranch(pos);
		if (PathToBranch.empty())
		{
			return this;
		}
		else
		{
			return WalkThePath(PathToBranch);
		}
	}
	
	virtual BaseHolder* WalkThePath(list<BranchDirection> &PathToBranch)
	{
		if (PathToBranch.empty())
		{
			return this;
		}
		BranchDirection Direction = PathToBranch.back();
		PathToBranch.pop_back();
		switch (Direction)
		{
		case BranchDirection::Left:
			if (LeftBranch)
			{
				return LeftBranch->WalkThePath(PathToBranch);
			}
			else
			{
				return nullptr;
			}
			break;
		case BranchDirection::Central:
			if (CentralBranch)
			{
				return CentralBranch->WalkThePath(PathToBranch);
			}
			else
			{
				return nullptr;
			}
			break;
		case BranchDirection::Right:
			if (RightBranch)
			{
				return RightBranch->WalkThePath(PathToBranch);
			}
			else
			{
				return nullptr;
			}
			break;
		}
	}

	list<BranchDirection> CalculatePathToBranch(int BranchNumber)
	{
		float tmp_BranchNumber = (float)BranchNumber;
		list<BranchDirection> PathToBranch;
		while (BranchNumber > 3)
		{
			tmp_BranchNumber /= 3;
			if (tmp_BranchNumber - (float)((int)tmp_BranchNumber) == 0)
			{
				PathToBranch.push_back(BranchDirection::Right);
				BranchNumber = (int)tmp_BranchNumber - 1;
			}
			else if (tmp_BranchNumber - (float)((int)tmp_BranchNumber) >= 0.6f)
			{
				PathToBranch.push_back(BranchDirection::Central);
				BranchNumber = (int)tmp_BranchNumber;
			}
			else
			{
				PathToBranch.push_back(BranchDirection::Left);
				BranchNumber = (int)tmp_BranchNumber;
			}
			tmp_BranchNumber = (float)((int)tmp_BranchNumber);
		}
		switch (BranchNumber)
		{
		case 1:
			PathToBranch.push_back(BranchDirection::Left);
			break;
		case 2:
			PathToBranch.push_back(BranchDirection::Central);
			break;
		case 3:
			PathToBranch.push_back(BranchDirection::Right);
			break;
		default:
			break;
		}
		return PathToBranch;
	}

	virtual const int& Find(const int& data)
	{
		if ((float)data == DataWeight)
		{
			if (CentralBranch)
			{
				return CentralBranch->Find(data);
			}
			else
			{
				throw std::string("Can't find data");
			}
		}
		else
		{
			if ((float)data > DataWeight)
			{
				if (RightBranch)
				{
					return RightBranch->Find(data);
				}
				else
				{
					throw std::string("Can't find data");
				}
			}
			else
			{
				if (LeftBranch)
				{
					return LeftBranch->Find(data);
				}
				else
				{
					throw std::string("Can't find data");
				}
			}
		}
	};
	virtual const char* Find(const char(&data)[10])
	{
		float tmp = 0.0f;
		for (int i = 0; i < 10; ++i)
		{
			tmp += (float)data[i];
		};

		if (tmp == DataWeight)
		{
			if (CentralBranch)
			{
				return CentralBranch->Find(data);
			}
			else
			{
				throw std::string("Can't find data");
			}
		}
		else
		{
			if (tmp > DataWeight)
			{
				if (RightBranch)
				{
					return RightBranch->Find(data);

				}
				else
				{
					throw std::string("Can't find data");
				}
			}
			else
			{
				if (LeftBranch)
				{
					return LeftBranch->Find(data);
				}
				else
				{
					throw std::string("Can't find data");
				}
			}
		}
	};
	virtual const float& Find(const float& data)
	{
		if (data == DataWeight)
		{
			if (CentralBranch)
			{
				return CentralBranch->Find(data);
			}
			else
			{
				throw std::string("Can't find data");
			}
		}
		else
		{
			if (data > DataWeight)
			{
				if (RightBranch)
				{
					return RightBranch->Find(data);
				}
				else
				{
					throw std::string("Can't find data");
				}
			}
			else
			{
				if (LeftBranch)
				{
					return LeftBranch->Find(data);
				}
				else
				{
					throw std::string("Can't find data");
				}
			}
		}
	};
	virtual const char* FindChar(const char* data)
	{
		const char* tmp = FormattingCharArray(data);
		char ref_tmp[10];
		for (int i = 0; i < 10; ++i)
		{
			ref_tmp[i] = *(tmp + i);
		}
		delete[] tmp;
		return Find(ref_tmp);

	};
	


	void Delete(const int& data);
	void Delete(const char(&data)[10]);
	void Delete(const float& data);


protected:

	BaseHolder* LeftBranch = nullptr;
	BaseHolder* CentralBranch = nullptr;
	BaseHolder* RightBranch = nullptr;

	float DataWeight;

};

class IntHolder : public BaseHolder
{
public:
	IntHolder(const int& data);
	const int& GetData() const;
	bool operator==(BaseHolder* other);
	const int& Find(const int& data) override;
private:
	int Data;
};

class CharrArrayHolder : public BaseHolder
{
public:
	CharrArrayHolder(const char(&data)[10]);

	const char* GetData() const;
	bool operator==(BaseHolder* other);
	const char* Find(const char(&data)[10]) override;
private:
	char Data[10];
};

class FloatHolder : public BaseHolder
{
public:
	FloatHolder(const float& data);

	const float& GetData() const;
	bool operator==(BaseHolder* other);
	const float& Find(const float& data) override;
private:
	float Data;
};


