#include "CurrentTypeHolders.h"

BaseHolder::BaseHolder()
{
	DataWeight = 100.0f;
}

const char* BaseHolder::FormattingCharArray(const char* data)
{

	int Size = 1;
	char* tmp = new char[10];

	for (int i = 0; *(data + i) != '\0'; ++i)
	{
		++Size;
	}
	if (Size >= 10)
	{
		for (size_t i = 0; i < 10; i++)
		{
			*(tmp + i) = *(data + i);
		}
	}
	else
	{
		for (size_t i = 0; i < 10; i++)
		{
			if (i < Size)
			{
				*(tmp + i) = *(data + i);
			}
			else
			{
				*(tmp + i) = 0;
			}
		}

	}

	std::cout << "size of array " << Size << std::endl;
	return tmp;
}

bool BaseHolder::Insert(BaseHolder* data)
{
	if (data->DataWeight == DataWeight)
	{
		if (!CentralBranch)
		{
			CentralBranch = data;
		}
		else
		{
			CentralBranch->Insert(data);
		}
	}
	else
	{
		if (data->DataWeight > DataWeight)
		{
			if (!RightBranch)
			{
				RightBranch = data;
			}
			else
			{
				RightBranch->Insert(data);
			}
		}
		else
		{
			if (!LeftBranch)
			{
				LeftBranch = data;
			}
			else
			{
				LeftBranch->Insert(data);
			}
		}
	}
	return true;
}

bool BaseHolder::Insert(const int& data)
{
	try
	{
		Find(data);
		return false;
	}
	catch (std::string e)
	{
		Insert(new IntHolder(data));
		std::cout << "Reduplicate data" << std::endl;
		return true;
	}
	catch (...)
	{
		return false;
	}
}

bool BaseHolder::InsertChar(const char(&data)[10])
{
	try
	{
		Find(data);
		std::cout << "Reduplicate data" << std::endl;
		return false;
	}
	catch (std::string e)
	{
		Insert(new CharrArrayHolder(data));
		return true;
	}
	catch (...)
	{
		return false;
	}
}

bool BaseHolder::Insert(const char* data)
{
	const char* tmp = FormattingCharArray(data);
	char ref_tmp[10];
	for (int i = 0; i < 10; ++i)
	{
		ref_tmp[i] = *(tmp + i);
	}
	delete[] tmp;
	bool bIsSuccess = this->InsertChar(ref_tmp);
	return bIsSuccess;
}

bool BaseHolder::Insert(const float& data)
{
	try
	{
		Find(data);
		std::cout << "Reduplicate data" << std::endl;
		return false;
	}
	catch (std::string e)
	{
		Insert(new FloatHolder(data));
		return true;
	}
	catch (...)
	{
		return false;
	}
}



IntHolder::IntHolder(const int& data)
{
	Data = data;
	DataWeight = (float)Data;
}

const int& IntHolder::GetData() const
{
	return Data;
}

bool IntHolder::operator==(BaseHolder* other)
{
	return this->Data == static_cast<IntHolder*>(other)->Data;
}

const int& IntHolder::Find(const int& data)
{
	if (data == Data)
	{
		return this->GetData();
	}
	else
	{
		BaseHolder::Find(data);
	}
}



CharrArrayHolder::CharrArrayHolder(const char(&data)[10])
{
	DataWeight = 0.0f;
	for (int i = 0; i < 10; ++i)
	{
		Data[i] = data[i];
		DataWeight += (float)Data[i];
	};
}

const char* CharrArrayHolder::GetData() const
{
	return Data;
}

bool CharrArrayHolder::operator==(BaseHolder* other)
{
	return this->Data == static_cast<CharrArrayHolder*>(other)->Data;
}

const char* CharrArrayHolder::Find(const char(&data)[10])
{
	bool bIsThisString = true;
	for (int i = 0; i < 10; i++)
	{
		bIsThisString = Data[i] == data[i];
		if (!bIsThisString)
		{
			break;
		}
	}
	if (bIsThisString)
	{
		return this->GetData();
	}
	else
	{
		BaseHolder::Find(data);
	}
}


FloatHolder::FloatHolder(const float& data)
{
	Data = data;
	DataWeight = Data;
}

const float& FloatHolder::GetData() const
{
	return Data;
}

bool FloatHolder::operator==(BaseHolder* other)
{
	return this->Data == static_cast<FloatHolder*>(other)->Data;
}

const float& FloatHolder::Find(const float& data)
{
	if (data == Data)
	{
		return this->GetData();
	}
	else
	{
		BaseHolder::Find(data);
	}
}

bool operator==(const IntHolder& me, BaseHolder* other)
{
	return me.GetData() == static_cast<IntHolder*>(other)->GetData();
}

bool operator==(const CharrArrayHolder& me, BaseHolder* other)
{
	return me.GetData() == static_cast<CharrArrayHolder*>(other)->GetData();
}

bool operator==(const FloatHolder& me, BaseHolder* other)
{
	return me.GetData() == static_cast<FloatHolder*>(other)->GetData();
}
